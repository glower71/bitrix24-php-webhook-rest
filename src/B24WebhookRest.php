<?php namespace Shilov\Bitrix24WebhookRest;

use Shilov\Bitrix24WebhookRest\Exceptions\CurlException;
use Shilov\Bitrix24WebhookRest\Exceptions\RequestException;

class B24WebhookRest
{
    protected $userId;
    protected $domain;
    protected $authCode;

    public function __construct(string $domain, string $userId, string $authCode)
    {
        $this->domain = $domain;
        $this->userId = $userId;
        $this->authCode = $authCode;
    }

    /**
     * @param $method
     * @param array $params
     * @return array
     * @throws RequestException
     * @throws CurlException
     */
    public function call($method, $params = []): array
    {
        $queryUrl = 'https://' . $this->domain . '/rest/' . $this->userId . '/' . $this->authCode . '/' . $method;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => http_build_query($params),
        ));

        $curlResult = curl_exec($curl);
        curl_close($curl);

        if($curlResult === false) {
            throw new CurlException('Curl request error');
        }

        $result = json_decode($curlResult, true);
        if(isset($result['error'])) {
            throw new RequestException($result['error_description']);
        }

        return $result;
    }
}