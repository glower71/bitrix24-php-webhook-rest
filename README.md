##bitrix24-php-webhook-rest
A simple PHP library for using Bitrix24 REST API via Webhook

[Bitrix24 Вебхуки](https://helpdesk.bitrix24.ru/open/5408147/)

[Bitrix24 API документация - Russian](http://dev.1c-bitrix.ru/rest_help/)<br />
[Bitrix24 API documentation - English](https://training.bitrix24.com/rest_help/)

##Requirements
- php: >=7.0.0
- ext-json: *
- ext-curl: *
        
## Example ##
``` php
use Shilov\Bitrix24WebhookRest\B24WebhookRest;

$rest = new B24WebhookRest($domain, $userId, $authCode);
$result = $rest->call('crm.lead.get', [
   'id' => 1
]);
```        
##Installation
    composer require shilov/bitrix24-php-webhook-rest:dev-master

##Author
Andrei Shilov - glower71@gmail.com - https://www.linkedin.com/in/shilov-a/        